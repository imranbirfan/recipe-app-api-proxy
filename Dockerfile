FROM nginxinc/nginx-unprivileged:1-alpine
LABEL maintainer="imranbirfan@gmail.com"

# Copy files from working directory to /etc/nginx
COPY ./default.conf.tpl /etc/nginx/default.conf.tpl
COPY ./uwsgi_params /etc/nginx/uwsgi_params

# Environment Variable Setup
ENV LISTEN_PORT=8000
ENV APP_HOST=app
ENV APP_PORT=9000

# Switch to root user
USER root

# Make a new directory, change the permissions of that directory for admin, group, and reader
# Make an empty default.conf file and change its ownership to the nginx user
RUN mkdir -p /vol/static
RUN chmod 755 /vol/static
RUN touch /etc/nginx/conf.d/default.conf
RUN chown nginx:nginx /etc/nginx/conf.d/default.conf

# Copy the entrypoint
COPY ./entrypoint.sh /entrypoint.sh

# Make the entrypoint executable by the user
RUN chmod +x /entrypoint.sh

# Switch back to nginx user
USER nginx

CMD ["/entrypoint.sh"]